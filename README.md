## Description

A front-end react app built with React, Redux, Redux Form, Redux Saga, and Material UI.

## Requirements
1. ```Nodejs: v10.12.0```
2. ```NPM: 6.4.1```
3. ```Yarn: 1.10.1```

## Installation
1. Clone the repo
2. Run ``` yarn install ``` or ``` npm install ```
4. Run the backend server built earlier: ```cd /api_directory && php bin/console server:run```
3. Run the dev server with: ```yarn dev-server````
4. Open ```http://localhost:8081/``` with the browser of your choice

If ```8081``` port is not working, checkout your terminal output. 
