import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Proptypes from 'prop-types';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';

import ProductListContainer from './ProductListContainer';
import ProductFormContainer from './ProductFormContainer';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  button: {
    margin: theme.spacing.unit
  },
  grow: {
    flexGrow: 1
  },

  leftIcon: {
    marginRight: theme.spacing.unit
  }
});

class ProductComponent extends React.Component {
  componentDidMount() {
    //this.props.fetchProducts(1, 10);
  }

  componentDidUpdate() {}

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <AppBar position="static" color="default">
            <Toolbar>
              <Typography variant="h6" color="inherit" className={classes.grow}>
                Simple React App
              </Typography>
              <Button
                variant="outlined"
                size="small"
                className={classes.button}
                onClick={() => this.props.showAddProductForm()}
              >
                <AddIcon
                  className={classNames(classes.leftIcon, classes.iconSmall)}
                />
                Add Product
              </Button>
            </Toolbar>
          </AppBar>
          <ProductListContainer />
          {this.props.products.showAddForm && <ProductFormContainer />}
        </Grid>
      </div>
    );
  }
}

ProductComponent.propTypes = {
  classes: Proptypes.object,
  fetchProducts: Proptypes.func.isRequired,
  showAddProductForm: Proptypes.func,
  products: Proptypes.object
};

export default withStyles(styles)(ProductComponent);
