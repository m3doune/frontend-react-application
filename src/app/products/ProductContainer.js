import { connect } from 'react-redux';
import ProductComponent from './ProductComponent';
import { productActions } from './duck';

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = dispatch => {
  const fetchProducts = (page = 1, limit = 10) => {
    dispatch(productActions.fetchProductList(page, limit));
  };

  const showAddProductForm = () => {
    dispatch(productActions.showAddProductForm());
  };

  const hideAddProductForm = () => {
    dispatch(productActions.hideAddProductForm());
  };
  return {
    fetchProducts,
    showAddProductForm,
    hideAddProductForm
  };
};

const ProductContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductComponent);

export default ProductContainer;
