import types from './types';

const productReducer = (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_PRODUCT_LIST_REQUESTED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: true,
          successful: false,
          error: false
        }
      };
    case types.FETCH_PRODUCT_LIST_SUCCEEDED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: false,
          successful: true,
          error: false,
          data: action.response.data
        }
      };
    case types.FETCH_PRODUCT_LIST_FAILED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: false,
          successful: false,
          error: true
        }
      };

    case types.FETCH_PRODUCT_ITEM_REQUESTED:
      return {
        ...state,
        selected_item: {
          ...state.selected_item,
          requesting: true,
          successful: false,
          error: false
        }
      };
    case types.FETCH_PRODUCT_ITEM_SUCCEEDED:
      return {
        ...state,
        selected_item: {
          ...state.selected_item,
          requesting: false,
          successful: true,
          error: false,
          data: action.response.data
        }
      };
    case types.FETCH_PRODUCT_ITEM_FAILED:
      return {
        ...state,
        selected_item: {
          ...state.selected_item,
          requesting: false,
          successful: false,
          error: true
        }
      };

    case types.SHOW_PRODUCT_FORM:
      return {
        ...state,
        showAddForm: true
      };

    case types.HIDE_PRODUCT_FORM:
      return {
        ...state,
        showAddForm: false
      };

    // --- SEARCH REDUCERS
    case types.SEARCH_PRODUCT_REQUESTED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: true,
          successful: false,
          error: false
        }
      };
    case types.SEARCH_PRODUCT_SUCCEEDED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: false,
          successful: true,
          error: false,
          data: action.response.data
        }
      };
    case types.SEARCH_PRODUCT_FAILED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: false,
          successful: false,
          error: true
        }
      };

    default:
      return state;
  }
};

export const brandReducer = (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_BRAND_LIST_REQUESTED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: true,
          successful: false,
          error: false
        }
      };
    case types.FETCH_BRAND_LIST_SUCCEEDED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: false,
          successful: true,
          error: false,
          data: action.response.data
        }
      };

    case types.FETCH_BRAND_LIST_FAILED:
      return {
        ...state,
        list: {
          ...state.list,
          requesting: false,
          successful: false,
          error: true
        }
      };
    default:
      return state;
  }
};

export default productReducer;
