import client from '../../commons/http_client';

const fetchProductList = (page = 1, limit = 10) => {
  return new Promise((resolve, reject) => {
    client
      .get(`/api/products?page=${page}&limit=${limit}`)
      .then(response => {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const createProduct = data => {
  return new Promise((resolve, reject) => {
    client
      .post('/api/products', data)
      .then(response => {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const updateProduct = (id, data) => {
  return new Promise((resolve, reject) => {
    client
      .patch(`/api/products/${id}`, data)
      .then(response => {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const deleteProduct = id => {
  return new Promise((resolve, reject) => {
    client
      .delete(`/api/products/${id}`)
      .then(response => {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const getSingleProduct = id => {
  return new Promise((resolve, reject) => {
    client
      .get(`/api/products/${id}`)
      .then(response => {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const searchProducts = (term = '', page = 1, limit = 10, sort = []) => {
  return new Promise((resolve, reject) => {
    client
      .get(
        `/api/products/search?term=${term}&page=${page}&limit=${limit}&sort=${sort.join(
          ','
        )}`
      )
      .then(response => {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const fetchBrands = (page = 1, limit = 10) => {
  return new Promise((resolve, reject) => {
    client
      .get(`/api/brands?page=${page}&limit=${limit}`)
      .then(response => {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

export default {
  fetchProductList,
  createProduct,
  updateProduct,
  deleteProduct,
  getSingleProduct,
  searchProducts,
  fetchBrands
};
