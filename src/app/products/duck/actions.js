import types from './types';

const fetchProductList = (page = 1, limit = 10) => {
  return {
    type: types.FETCH_PRODUCT_LIST_REQUESTED,
    page,
    limit
  };
};

const fetchProductListSucceded = response => {
  return {
    type: types.FETCH_PRODUCT_LIST_SUCCEEDED,
    response
  };
};

const fetchProductListFailed = error => {
  return {
    type: types.FETCH_PRODUCT_LIST_FAILED,
    error
  };
};

const fetchProductItem = id => {
  return {
    type: types.FETCH_PRODUCT_ITEM_REQUESTED,
    id
  };
};

const fetchProductItemSucceded = response => {
  return {
    type: types.FETCH_PRODUCT_ITEM_SUCCEEDED,
    response
  };
};

const fetchProductItemFailed = error => {
  return {
    type: types.FETCH_PRODUCT_ITEM_FAILED,
    error
  };
};

const searchProductsRequested = (
  term = '',
  page = 1,
  limit = 10,
  sort = []
) => {
  return {
    type: types.SEARCH_PRODUCT_REQUESTED,
    term,
    page,
    limit,
    sort
  };
};
const searchProductsFailed = error => {
  return {
    type: types.SEARCH_PRODUCT_FAILED,
    error
  };
};
const searchProductsSucceded = response => {
  return {
    type: types.SEARCH_PRODUCT_SUCCEEDED,
    response
  };
};

//Brands

const fetchBrandList = (page = 1, limit = 10) => {
  return {
    type: types.FETCH_BRAND_LIST_REQUESTED,
    page,
    limit
  };
};

const fetchBrandListSucceded = response => {
  return {
    type: types.FETCH_BRAND_LIST_SUCCEEDED,
    response
  };
};

const fetchBrandListFailed = error => {
  return {
    type: types.FETCH_BRAND_LIST_FAILED,
    error
  };
};

const showAddProductForm = () => {
  return {
    type: types.SHOW_PRODUCT_FORM
  };
};
const hideAddProductForm = () => {
  return {
    type: types.HIDE_PRODUCT_FORM
  };
};

const submitProductForm = data => {
  return {
    type: types.SUBMIT_PRODUCT_FORM,
    data
  };
};

const createProductSucceded = response => {
  return {
    type: types.CREATE_PRODUCT_SUCCEEDED,
    response,
    message: 'Product created successfully'
  };
};

export default {
  //List
  fetchProductList,
  fetchProductListSucceded,
  fetchProductListFailed,
  //Items
  fetchProductItem,
  fetchProductItemSucceded,
  fetchProductItemFailed,

  createProductSucceded,

  //FORM
  showAddProductForm,
  hideAddProductForm,
  submitProductForm,

  //Search
  searchProductsRequested,
  searchProductsFailed,
  searchProductsSucceded,

  //Brands
  fetchBrandList,
  fetchBrandListSucceded,
  fetchBrandListFailed
};
