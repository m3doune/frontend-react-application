//PRODUCT LIST

const FETCH_PRODUCT_LIST_REQUESTED = 'FETCH_PRODUCT_LIST_REQUESTED';
const FETCH_PRODUCT_LIST_SUCCEEDED = 'FETCH_PRODUCT_LIST_SUCCEEDED';
const FETCH_PRODUCT_LIST_FAILED = 'FETCH_PRODUCT_LIST_FAILED';

//PRODUCT ITEM
const FETCH_PRODUCT_ITEM_REQUESTED = 'FETCH_PRODUCT_ITEM_REQUESTED';
const FETCH_PRODUCT_ITEM_SUCCEEDED = 'FETCH_PRODUCT_ITEM_SUCCEEDED';
const FETCH_PRODUCT_ITEM_FAILED = 'FETCH_PRODUCT_ITEM_FAILED';

const CREATE_PRODUCT_SUCCEEDED = 'CREATE_PRODUCT_SUCCEEDED';

//SEARCH PRODUCT
const SEARCH_PRODUCT_REQUESTED = 'SEARCH_PRODUCT_REQUESTED';
const SEARCH_PRODUCT_SUCCEEDED = 'SEARCH_PRODUCT_SUCCEEDED';
const SEARCH_PRODUCT_FAILED = 'SEARCH_PRODUCT_FAILED';

//FORMS
const SHOW_PRODUCT_FORM = 'SHOW_PRODUCT_FORM';
const HIDE_PRODUCT_FORM = 'HIDE_PRODUCT_FORM';
const SUBMIT_PRODUCT_FORM = 'SUBMIT_PRODUCT_FORM';

//BRANDS
const FETCH_BRAND_LIST_REQUESTED = 'FETCH_BRAND_LIST_REQUESTED';
const FETCH_BRAND_LIST_SUCCEEDED = 'FETCH_BRAND_LIST_SUCCEEDED';
const FETCH_BRAND_LIST_FAILED = 'FETCH_BRAND_LIST_FAILED';

export default {
  FETCH_PRODUCT_LIST_REQUESTED,
  FETCH_PRODUCT_LIST_SUCCEEDED,
  FETCH_PRODUCT_LIST_FAILED,
  FETCH_PRODUCT_ITEM_REQUESTED,
  FETCH_PRODUCT_ITEM_SUCCEEDED,
  FETCH_PRODUCT_ITEM_FAILED,
  CREATE_PRODUCT_SUCCEEDED,
  SHOW_PRODUCT_FORM,
  HIDE_PRODUCT_FORM,
  SEARCH_PRODUCT_REQUESTED,
  SEARCH_PRODUCT_SUCCEEDED,
  SEARCH_PRODUCT_FAILED,
  FETCH_BRAND_LIST_REQUESTED,
  FETCH_BRAND_LIST_SUCCEEDED,
  FETCH_BRAND_LIST_FAILED,
  SUBMIT_PRODUCT_FORM
};
