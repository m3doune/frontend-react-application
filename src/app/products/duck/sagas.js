import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import types from './types';
import operations from './operations';
import actions from './actions';

function* getProducts(action) {
  try {
    const { page, limit } = action;
    const response = yield call(operations.fetchProductList, page, limit);
    yield put(actions.fetchProductListSucceded(response));
  } catch (error) {
    yield put(actions.fetchProductListFailed(error));
  }
}

function* searchProducts(action) {
  try {
    const { term, page, limit, sort } = action;
    var pageV = page == 0 ? 1 : page;
    const response = yield call(
      operations.searchProducts,
      term,
      pageV,
      limit,
      sort
    );
    yield put(actions.searchProductsSucceded(response));
  } catch (error) {
    yield put(actions.searchProductsFailed(error));
  }
}

function* createProduct(action) {
  try {
    const { data } = action;
    const response = yield call(operations.createProduct, data);
    yield put(actions.fetchProductListSucceded(response));
  } catch (error) {
    yield put(actions.fetchProductListFailed(error));
  }
}

export default function* productsWatcher() {
  yield takeEvery(types.FETCH_PRODUCT_LIST_REQUESTED, getProducts);
  yield takeEvery(types.SEARCH_PRODUCT_REQUESTED, searchProducts);
  yield takeLatest(types.SUBMIT_PRODUCT_FORM, createProduct);
}
