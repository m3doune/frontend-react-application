import { connect } from 'react-redux';
import ProductListComponent from './ProductListComponent';
import { productActions } from './duck';

const mapStateToProps = state => {
  const {
    current_page_number,
    num_items_per_page,
    total_count,
    items
  } = state.products.list.data;

  return {
    current_page: current_page_number,
    number_of_items_per_page: num_items_per_page,
    count: total_count,
    items
  };
};

const mapDispatchToProps = dispatch => {
  const fetchProducts = (page = 1, limit = 10) => {
    dispatch(productActions.fetchProductList(page, limit));
  };

  const searchProduct = (term, page = 1, limit = 10, sort = []) => {
    dispatch(productActions.searchProductsRequested(term, page, limit, sort));
  };

  return { fetchProducts, searchProduct };
};

const ProductListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductListComponent);

export default ProductListContainer;
