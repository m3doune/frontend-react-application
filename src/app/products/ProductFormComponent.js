import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CloseIcon from '@material-ui/icons/Close';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Proptypes from 'prop-types';

let RenderInput = field => (
  <TextField
    id={field.id}
    className={field.classes.textField}
    label={field.label}
    fullWidth
    {...field.input}
    type={field.type || 'text'}
    inputProps={field.input}
    variant="outlined"
    {...(field.meta.touched && field.meta.error
      ? { error: true, helperText: field.meta.error }
      : {})}
  />
);

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  button: {
    margin: theme.spacing.unit
  }
});

let ProductForm = props => {
  const { createProduct, classes, handleBrandChange } = props;
  console.log(props);
  return (
    <Grid item xs>
      <Card>
        <CardHeader
          title={'Add a new product'}
          action={
            <IconButton
              onClick={() => {
                props.closeForm();
              }}
            >
              <CloseIcon />
            </IconButton>
          }
        />
        <CardContent>
          <Grid container spacing={24}>
            <form
              onSubmit={createProduct}
              className={classes.container}
              noValidate
              autoComplete="off"
            >
              <Grid item xs={12} sm={12} md={12}>
                <Field
                  component={RenderInput}
                  label={'Name'}
                  name={'name'}
                  required={true}
                  fullWidth={true}
                  classes={classes.textField}
                  margin={'dense'}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Field
                  component={RenderInput}
                  label={'Description'}
                  name={'description'}
                  required={true}
                  fullWidth={true}
                  type={'textarea'}
                  multiline={true}
                  classes={classes.textField}
                  rows={3}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <FormControl className={classes.formControl} variant="outlined">
                  <InputLabel htmlFor="brand-simple">Brand</InputLabel>
                  <Select
                    value={10}
                    onChange={handleBrandChange}
                    fullWidth
                    input={
                      <OutlinedInput
                        labelWidth={30}
                        name="brand"
                        id="brand-simple"
                      />
                    }
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {props.brand.items.map(e => {
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Button
                variant="outlined"
                type={'submit'}
                className={classes.button}
              >
                Submit
              </Button>
            </form>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );
};

ProductForm.propTypes = {
  closeForm: Proptypes.func,
  createProduct: Proptypes.func
};

ProductForm = reduxForm({
  form: 'product'
})(ProductForm);

export default withStyles(styles)(ProductForm);
