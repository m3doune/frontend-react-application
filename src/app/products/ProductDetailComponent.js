import React from 'react';
import Proptypes from 'prop-types';

function ProductDetailComponent({ name, url, description, brand, category }) {
  return (
    <div>
      <h1>{name}</h1>
      <ul>
        <li key={1}>Description: {description} </li>
        <li key={2}>Brand: {brand} </li>
        <li key={3}>Category: {category} </li>
        <li key={4}>Url: {url} </li>
      </ul>
    </div>
  );
}

ProductDetailComponent.propTypes = {
  name: Proptypes.string.isRequired,
  url: Proptypes.string,
  description: Proptypes.string,
  brand: Proptypes.string,
  category: Proptypes.string
};

export default ProductDetailComponent;
