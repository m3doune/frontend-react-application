import React from 'react';
import Proptypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  root: {
    flexGrow: 1
  }
});

const transformProducts = (items = []) => {
  return items.map(product => {
    var categories = product.categories.map(c => c.name).join(',');
    return [
      product.name,
      product.description,
      categories,
      product.brand.name,
      product.url
    ];
  });
};

function debounce(a, b, c) {
  var d, e;
  return function() {
    function h() {
      (d = null), c || (e = a.apply(f, g));
    }
    var f = this,
      g = arguments;
    return (
      clearTimeout(d), (d = setTimeout(h, b)), c && !d && (e = a.apply(f, g)), e
    );
  };
}

const columns = [
  {
    name: 'Product Name',
    options: {
      filter: false
    }
  },
  {
    name: 'Description',
    options: {
      filter: false
    }
  },
  {
    name: 'Category',
    options: {
      filter: true
    }
  },

  {
    name: 'Brand',
    options: {
      filter: true
    }
  },
  {
    name: 'URL',
    options: {
      filter: false
    }
  }
];

class ProductListComponent extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchProducts(
      this.props.current_page,
      this.props.number_of_items_per_page
    );
  }

  render() {
    const { classes } = this.props;
    const options = {
      serverSide: true,
      filterType: 'dropdown',
      download: false,
      print: false,
      rowsPerPage: this.props.number_of_items_per_page || 10,
      count: this.props.count,
      page: this.props.current_page,
      onTableChange: (action, tableState) => {
        console.log(action, tableState);
        // a developer could react to change on an action basis or
        // examine the state as a whole and do whatever they want

        switch (action) {
          case 'changePage':
            this.props.fetchProducts(
              tableState.page,
              this.props.number_of_items_per_page
            );
            break;

          case 'changeRowsPerPage':
            this.props.fetchProducts(tableState.page, tableState.rowsPerPage);
            break;

          case 'sort':
            //this.props.searchProduct()
            break;

          case 'filterChange':
            break;
          default:
            break;
        }
      }
    };

    return (
      <Grid item xs>
        <Paper>
          <MUIDataTable
            title={'Products List'}
            columns={columns}
            options={options}
            data={transformProducts(this.props.items)}
          />
        </Paper>
      </Grid>
    );
  }
}

ProductListComponent.propTypes = {
  current_page: Proptypes.number,
  number_of_items_per_page: Proptypes.number,
  count: Proptypes.number,
  items: Proptypes.array,
  classes: Proptypes.any,
  fetchProducts: Proptypes.func,
  searchProduct: Proptypes.func
};

export default withStyles(styles)(ProductListComponent);
