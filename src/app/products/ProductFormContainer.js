import { connect } from 'react-redux';
import ProductFormComponent from './ProductFormComponent';
import { productActions } from './duck';

const mapStateToProps = state => {
  const {
    current_page_number,
    num_items_per_page,
    total_count,
    items
  } = state.brands.list.data;

  return {
    brand: {
      current_page: current_page_number,
      number_of_items_per_page: num_items_per_page,
      count: total_count,
      items
    }
  };
};

const mapDispatchToProps = dispatch => {
  const fetchBrands = (page = 1, limit = 10) => {
    dispatch(productActions.fetchProductList(page, limit));
  };
  const closeForm = () => {
    dispatch(productActions.hideAddProductForm());
  };

  const createProduct = data => {
    dispatch(productActions.submitProductForm(data));
  };

  return { fetchBrands, closeForm, createProduct };
};

const ProductFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductFormComponent);

export default ProductFormContainer;
