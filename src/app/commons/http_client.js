import axios from 'axios';

const baseURL = 'http://127.0.0.1:8000';

const defaultHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

const instance = axios.create({
  baseURL,
  timeout: 20000,
  headers: defaultHeaders
});

instance.interceptors.request.use(
  config => {
    // Do something before request is sent
    return config;
  },
  error =>
    // Do something with request error
    Promise.reject(error)
);

export function handleApiError(promise, resolve, reject) {
  return promise
    .then(response => {
      if (response.error) {
        reject(response);
      } else {
        resolve(response);
      }
    })
    .catch(error => {
      reject(error.response);
    });
}

export default instance;
