import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import 'regenerator-runtime/runtime';

import reducers from './reducers';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();

export const INITIAL_STATE = {
  products: {
    list: {
      requesting: false,
      successful: false,
      error: false,
      data: {
        current_page_number: 1,
        num_items_per_page: 10,
        total_count: 0,
        items: []
      }
    },
    showAddForm: false
  },
  brands: {
    list: {
      requesting: false,
      successful: false,
      error: false,
      data: {
        current_page_number: 1,
        num_items_per_page: 10,
        total_count: 0,
        items: []
      }
    }
  }
};

const store = createStore(
  reducers,
  INITIAL_STATE,
  compose(
    applyMiddleware(sagaMiddleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

sagaMiddleware.run(sagas);

import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
