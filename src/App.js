import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import ProductContainer from './app/products';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={ProductContainer} />
          <Route exact path="/products" component={ProductContainer} />
        </div>
      </Router>
    );
  }
}

export default App;
