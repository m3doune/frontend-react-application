import { all, fork } from 'redux-saga/effects';

import { productSagas } from './app/products/duck';

function* rootSaga() {
  yield all([fork(productSagas)]);
}

export default rootSaga;
