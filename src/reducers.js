import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import productReducer, { brandReducer } from './app/products/duck/reducers';

const rootReducer = combineReducers({
  products: productReducer,
  brands: brandReducer,
  form: formReducer
});

export default rootReducer;
